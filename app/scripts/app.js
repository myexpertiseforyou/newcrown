'use strict';

/**
 * @ngdoc overview
 * @name newcrownApp
 * @description
 * # newcrownApp
 *
 * Main module of the application.
 */
angular
  .module('newcrownApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap-slider'
  ])
  .config(function($routeProvider, $stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('home', {
        url: '/',
        templateUrl: '/views/core/main.html',
        controller: 'MainCtrl'
      })
      .state('hiw', {
        url: '/hiw',
        templateUrl: '/views/core/howitworks.html',
        controller: 'MainCtrl'
      })
      .state('interest', {
        url: '/interest',
        templateUrl: '/views/core/interestRates.html',
        controller: 'MainCtrl'
      })

    .state('signin', {
      url: '/signin',
      templateUrl: '/views/auth/signin.html',
      controller: 'MainCtrl'
    })

    .state('form', {
      url: '/form',
      views: {
        '': {
          templateUrl: 'views/core/forms/form.html'
        },



        'tab1@form': {
          templateUrl: "views/core/forms/registration.html"
        },
        'tab2@form': {
          templateUrl: "views/core/forms/deposit.html"
        },
        'tab3@form': {
          templateUrl: "views/core/forms/withdrawalCheck.html"
        },
        'tab4@form': {
          templateUrl: "views/core/forms/withdrawlWireTransfer.html"
        }


      }
    })

    .state('user', {
      url: '/user',
      abstract: true,
      views: {
        '': {
          templateUrl: 'views/user/partials/layout.html'
        },
        'header@user': {
          templateUrl: 'views/user/partials/header.html'
        },

        'footer@user': {
          templateUrl: 'views/user/partials/footer.html'
        }
      }
    })



    .state('user.account', {
      url: '/account',
      views: {
        'mainView': {
          templateUrl: '/views/user/accounts.html'
        }
      }

    })

    $urlRouterProvider.otherwise('/');
  });