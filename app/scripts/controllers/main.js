'use strict';

/**
 * @ngdoc function
 * @name newcrownApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the newcrownApp
 */
angular.module('newcrownApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
