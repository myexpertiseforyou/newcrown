'use strict';

/**
 * @ngdoc function
 * @name newcrownApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the newcrownApp
 */
angular.module('newcrownApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
