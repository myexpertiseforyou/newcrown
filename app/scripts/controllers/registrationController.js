'use strict';

/**
 * @ngdoc function
 * @name registrationController
 * @description
 */

angular.module('newcrownApp')
  .controller("registrationController", function($rootScope, $scope, $state) {

    $scope.tabs = [{
        title: "",
        route: "tab1",
        active: false
      }, {
        title: "",
        route: "tab2",
        active: false
      }, {
        title: "",
        route: "tab3",
        active: false
      },

      {
        title: "",
        route: "tab4",
        active: true
      }
    ];



  });